﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParsingAndHandling
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = 0;
            while (n == 0)
            {
                string Areashape = "";
                Console.WriteLine("Enter a number corresponding to the shape:\n" +
                                "1.SphereArea\n" +
                                "2.CubeArea\n" +
                                "3.CylinderArea\n" +
                                "4.TriangleArea\n");
                try {
                    Areashape = Console.ReadLine();
                }
                catch (Exception e) {
                    Console.WriteLine("" + e.Message);
                }
                switch (Areashape)
                {
                    case "1":
                        Sphere s = new Sphere();
                        string rad = Console.ReadLine();
                        Console.WriteLine("Area:" + s.Area(rad));
                        n = 1;
                        break;
                    case "2":
                        Cube c = new Cube();
                        string side = Console.ReadLine();
                        Console.WriteLine("Area:" + c.Area(side));
                        n = 1;
                        break;
                    case "3":
                        Cylinder cy = new Cylinder();
                        string radius = Console.ReadLine();
                        string height = Console.ReadLine();
                        Console.WriteLine("Area:" + cy.Area(radius, height));
                        n = 1;
                        break;
                    case "4":
                        Triangle t = new Triangle();
                        string bas = Console.ReadLine();
                        string hei = Console.ReadLine();
                        Console.WriteLine("Area:" + t.Area(bas, hei));
                        n = 1;
                        break;
                    default:
                        Console.WriteLine("Enter valid number!!");
                        break;

                }
            }
        }
    }
    public abstract class Shape1Input{
        abstract public double Area(string r);
}
    public abstract class Shape2Input {
        abstract public double Area(string a, string b);
    }
    public class Sphere:Shape1Input
    {
        const double pi = 3.1459265;
        public override double Area(string radius) {
            bool B = false;
            double R = 0;
                B = Double.TryParse(radius, out R); 
            if (B == true)
            {
                double Area = 4 * pi * R * R;
                return Area;
            }
            else {
                Console.WriteLine("Enter valid numbers!!");
            }
            return 0;
        }
    }
    public class Cube : Shape1Input
    {
        public override double Area(string side) {
            bool B = false;
            double S = 0;
                B = Double.TryParse(side, out S);
            if (B == true)
            {
                double Area = 6 * S*S;
                return Area;
            }
            else
            {
                Console.WriteLine("Enter valid numbers!!");
            }

            return 0;
        }
}
    public class Cylinder : Shape2Input
{
        const double pi = 3.1459265;
        public override double Area(string R, string H) {
            bool B1 = false, B2 =  false;
            double rad = 0, hei = 0;
                B1 = Double.TryParse(R, out rad);
                B2 = Double.TryParse(H, out hei);
            if (B1 == true && B2 == true)
            {
                double Area = 2 * pi * rad * (rad + hei);
                return Area;
            }
            else
            {
                Console.WriteLine("Enter valid numbers!!");
            }

            return 0;
        }

    }
    public class Triangle : Shape2Input {
        public override double Area(string R, string H)
        {
            bool B1 = false, B2 = false;
            double rad = 0, hei = 0;
                B1 = Double.TryParse(R, out rad);
                B2 = Double.TryParse(H, out hei);
            if (B1 == true && B2 == true)
            {
                double Area = 0.5 * rad * hei;
                return Area;
            }
            else
            {
                Console.WriteLine("Enter valid numbers!!");
            }

            return 0;
        }


    }

}
