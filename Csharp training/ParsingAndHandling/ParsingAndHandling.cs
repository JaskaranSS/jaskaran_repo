﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParsingAndHandling
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = 0;
            while (n == 0)
            {
                string str = "";
                Cylinder c = new Cylinder();
                Console.WriteLine("Enter a number corresponding to the shape:\n" +
                                "1.SphereArea\n" +
                                "2.CubeArea\n" +
                                "3.CylinderArea\n");
                try {
                    str = Console.ReadLine();
                }
                catch (Exception e) {
                    Console.WriteLine(""+e.Message);
                }
                switch (str)
                {
                    case "1":
                        string rad = Console.ReadLine();
                        Console.WriteLine("Area:" + c.SphereArea(rad));
                        n = 1;
                        break;
                    case "2":
                        string side = Console.ReadLine();
                        Console.WriteLine("Area:" + c.CubeArea(side));
                        n = 1;
                        break;
                    case "3":
                        string radius = Console.ReadLine();
                        string height = Console.ReadLine();
                        Console.WriteLine("Area:" + c.CylinderArea(radius, height));
                        n = 1;
                        break;
                    default:
                        Console.WriteLine("Enter valid number!!");
                        break;

                }
            }
        }
    }
    
    public class Sphere
    {
        const double pi = 3.1459265;
        public double SphereArea(string radius) {
            bool B = false;
            double R = 0;
            try {
                B = Double.TryParse(radius, out R); 
            }
            catch (Exception e) {
                Console.WriteLine(""+e.Message);
            }
            if (B == true)
            {
                double Area = 4 * pi * R * R;
                return Area;
            }
            else {
                Console.WriteLine("Enter valid numbers!!");
            }
            return 0;
        }
    }
    public class Cube : Sphere
    {
        public double CubeArea(string side) {
            bool B = false;
            double S = 0;
            try {
                B = Double.TryParse(side, out S);
            }
            catch (Exception e) {
                Console.WriteLine("" + e.Message);
            }
            if (B == true)
            {
                double Area = 6 * S*S;
                return Area;
            }
            else
            {
                Console.WriteLine("Enter valid numbers!!");
            }

            return 0;
        }
}
    public class Cylinder : Cube
{
        const double pi = 3.1459265;
        public double CylinderArea(string R, string H) {
            bool B1 = false, B2 =  false;
            double rad = 0, hei = 0;
            try {
                B1 = Double.TryParse(R, out rad);
                B2 = Double.TryParse(H, out hei);
            }
            catch (Exception e) {
                Console.WriteLine("" + e);
            }
            if (B1 == true && B2 == true)
            {
                double Area = 2 * pi * rad * (rad + hei);
                return Area;
            }
            else
            {
                Console.WriteLine("Enter valid numbers!!");
            }

            return 0;
        }

}

}
