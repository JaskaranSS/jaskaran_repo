﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParsingAndHandling
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = 0;
            while (n == 0)
            {
                string str = "";
                Console.WriteLine("Enter a number corresponding to the shape:\n" +
                                "1.SphereArea\n" +
                                "2.CubeArea\n" +
                                "3.CylinderArea\n" +
                                "4.TriangleArea\n");
                try {
                    str = Console.ReadLine();
                }
                catch (Exception e) {
                    Console.WriteLine("" + e.Message);
                }
                switch (str)
                {
                    case "1":
                        Sphere s = new Sphere();
                        string rad = Console.ReadLine();
                        Console.WriteLine("Area:" + s.Area(rad));
                        n = 1;
                        break;
                    case "2":
                        Cube c = new Cube();
                        string side = Console.ReadLine();
                        Console.WriteLine("Area:" + c.Area(side));
                        n = 1;
                        break;
                    case "3":
                        Cylinder cy = new Cylinder();
                        string radius = Console.ReadLine();
                        string height = Console.ReadLine();
                        Console.WriteLine("Area:" + cy.Area(radius, height));
                        n = 1;
                        break;
                    case "4":
                        Triangle t = new Triangle();
                        string bas = Console.ReadLine();
                        string hei = Console.ReadLine();
                        Console.WriteLine("Area:" + t.Area(bas, hei));
                        n = 1;
                        break;
                    default:
                        Console.WriteLine("Enter valid number!!");
                        break;

                }
            }
        }
    }
    interface IShape1Input{
        double Area(string r);
}
    interface IShape2Input {
        double Area(string a, string b);
    }
    public class Sphere:Shape1Input
    {
        const double pi = 3.1459265;
        public double Area(string radius) {
            bool B = false;
            double R = 0;
            try {
                B = Double.TryParse(radius, out R); 
            }
            catch (Exception e) {
                Console.WriteLine(""+e.Message);
            }
            if (B == true)
            {
                double Area = 4 * pi * R * R;
                return Area;
            }
            else {
                Console.WriteLine("Enter valid numbers!!");
            }
            return 0;
        }
    }
    public class Cube : Shape1Input
    {
        public double Area(string side) {
            bool B = false;
            double S = 0;
            try {
                B = Double.TryParse(side, out S);
            }
            catch (Exception e) {
                Console.WriteLine("" + e.Message);
            }
            if (B == true)
            {
                double Area = 6 * S*S;
                return Area;
            }
            else
            {
                Console.WriteLine("Enter valid numbers!!");
            }

            return 0;
        }
}
    public class Cylinder : Shape2Input
{
        const double pi = 3.1459265;
        public double Area(string R, string H) {
            bool B1 = false, B2 =  false;
            double rad = 0, hei = 0;
            try {
                B1 = Double.TryParse(R, out rad);
                B2 = Double.TryParse(H, out hei);
            }
            catch (Exception e) {
                Console.WriteLine("" + e);
            }
            if (B1 == true && B2 == true)
            {
                double Area = 2 * pi * rad * (rad + hei);
                return Area;
            }
            else
            {
                Console.WriteLine("Enter valid numbers!!");
            }

            return 0;
        }

    }
    public class Triangle : Shape2Input {
        public double Area(string R, string H)
        {
            bool B1 = false, B2 = false;
            double rad = 0, hei = 0;
            try
            {
                B1 = Double.TryParse(R, out rad);
                B2 = Double.TryParse(H, out hei);
            }
            catch (Exception e)
            {
                Console.WriteLine("" + e);
            }
            if (B1 == true && B2 == true)
            {
                double Area = 0.5 * rad * hei;
                return Area;
            }
            else
            {
                Console.WriteLine("Enter valid numbers!!");
            }

            return 0;
        }


    }

}
