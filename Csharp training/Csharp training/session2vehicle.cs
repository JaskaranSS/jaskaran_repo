﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Csharp_training
{
    public enum Noofwheels { two = 2 , four = 4}
    class Program
    {
        static void Main(string[] args)
        {
            Bike car = new Bike("AMG", "Black", 200);
            car.Colour = "BlackGold";
            car.start();
            car.speedup(50);
            int toll;
            car.CalToll(out toll);
            Console.WriteLine("Toll amount is:{0}", toll);
            car.speedup(60);
            car.stop();

        }
    }
    public interface IPainter {
        void Paint();
    }
    public interface ISeatCover {
        void SeatCoverChanged();
    }
    public class Vehicle:IPainter
    {
        public void Paint() {
            Console.WriteLine("We will get in touch shortly.");
        }
        //abstract public void CalToll(out int toll);
        private string Name;
        public int speed = 0;
        public string Colour;

        readonly int Maxspeed = 100;
        public Vehicle(string Name, string colour, int Maxspeed) {
            this.Name = Name;
            this.Colour = colour;
            this.Maxspeed = Maxspeed;
        }
        public void start() {
            Console.WriteLine("The vehicle started ======3333");
        }
        public void stop() {
            Console.WriteLine("The vehicle stopped");
        }
        public void speedup(int speed)
        {
            this.speed += speed;
            if (this.speed < Maxspeed)
            {
                Console.WriteLine("Current speed is {0} kmph", this.speed);
            }
            else Console.WriteLine("The speedup will go over the max speed(100kmph) please choose a lower value!!");
        }
    }
    sealed class Bike : Vehicle,ISeatCover
    {
        public Bike(string Name, string colour, int Maxspeed) : base(Name, colour, Maxspeed) { 
        
        }
        public void CalToll(out int toll) {
            toll = 50 * (int)Noofwheels.two;
        }

        public void SeatCoverChanged()
        {
            Console.WriteLine("Our service men will get in touch with you shortly.");
        }
    }
    sealed class Car : Vehicle,ISeatCover
    {
        public Car(string Name, string colour,  int Maxspeed) : base(Name, colour,  Maxspeed)
        {

        }
        public void CalToll(out int toll)
        {
            toll = 100 * (int)Noofwheels.four;
        }

        public void SeatCoverChanged()
        {
            Console.WriteLine("Our service men will get in touch with you shortly.");
        }
    }
}
