﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FileHandling
{
    public class Data {
        public void WriteData(string path) {
            FileStream streamer = new FileStream(path, FileMode.Open, FileAccess.Write);
            StreamWriter writer = new StreamWriter(streamer);
            Console.WriteLine("Enter text to write:\n");
            writer.Write(Console.ReadLine());
            writer.Flush();
            writer.Close();
            streamer.Close();
        }
        public void ReadData(string path) {
            FileStream Fs = new FileStream(path, FileMode.Open, FileAccess.Read);
            StreamReader Reader = new StreamReader(Fs);
            Console.WriteLine("Text from file:\n");
            Reader.BaseStream.Seek(0, SeekOrigin.Begin);
            string filecontent = Reader.ReadToEnd();
            Console.WriteLine(filecontent);
            Fs.Close();
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            string fname = @"E:\vs_work\text.txt";
            if (File.Exists(fname)) File.Delete(fname);
            FileStream fcreate = File.Create(fname);
            fcreate.Close();
            Data data = new Data();
            data.WriteData("E:\\vs_work\\text.txt");
            data.ReadData("E:\\vs_work\\text.txt");
        }
    }
}
