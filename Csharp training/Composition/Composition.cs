﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Composition
{
    class Program
    {
        static void Main(string[] args)
        {
            NationalAnthem USanthem = new NationalAnthem("United States","The Star Spangled Banner",
                                                   "Francis Scott Key and John Stafford Smith",
                                                   1814);
            MusicalComposition UScomposition = new MusicalComposition("The Star Spangled Banner",
                                                          "Francis Scott Key and John Stafford Smith",
                                                          1814);
            USanthem.Display();
            UScomposition.Display();

        }
    }
    public class MusicalComposition
    {
        string Title;
        string Composer;
        int Year;

        public MusicalComposition(string Title, string Composer, int Year){
            this.Title = Title;
            this.Composer = Composer;
            this.Year = Year;
            
        }
        public virtual void Display() {
            Console.WriteLine("Musical Composition Details:\n" +
                              "Title:{0}\n" +
                              "Composer:{1}\n" +
                              "Year:{2}\n", Title, Composer, Year);
        }
    }

    public class NationalAnthem:MusicalComposition {
        string Name;
        public NationalAnthem(string Name, string Title, string Composer, int Year) : base(Title, Composer, Year) {
            this.Name = Name;
            
        }
        public override void Display() {
            Console.WriteLine("Nation Name: " + Name);
        }
    }
}
