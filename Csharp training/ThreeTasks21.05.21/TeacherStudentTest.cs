﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThreeTasks21._05._21
{
    class Program
    {
        static void Main(string[] args)
        {
            Person P = new Person();
            P.age = 20;
            P.ShowAge();
            Student S = new Student();
            S.age = 21;
            S.ShowAge();
            S.GoToClasses();
            Teacher T = new Teacher();
            T.age = 30;
            T.ShowAge();
            T.Explain();



        }
    }
    public class Person
    {
        public int age
        {
            get;
            set;
        }
        public Person(){
            Console.WriteLine("Hello\n");
}
        public virtual void ShowAge() {
            Console.WriteLine("I am Person\n "+"My age is " + age + " years old.");
        }
    }

        public class Student : Person { 
            public void GoToClasses() {
                Console.WriteLine("I'm going to class.");
            }
            public override void ShowAge() {
                Console.WriteLine("I am Student\n " + "My age is " + base.age + " years old.");
            }
        }
        public class Teacher : Person { 
            private string subject {
                get;set;
            }
            public void Explain() {
                Console.WriteLine("Explanation begins.");
            }
        public override void ShowAge()
        {
            Console.WriteLine("I am Teacher\n " + "My age is " + base.age + " years old.");
        }

    }
    }
