﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThreeTasks21._05._21
{

    public class Library
    {
        public struct Book
        {
            public string Title;
            public string Author;
        }
        Book[] Lib;
        public Library()
        { 
            Lib = new Book[1000]; 
        }
            
        public void AddBook(string title, string author) 
            {
            int BookNumber = 0;
            Lib[BookNumber].Title = title;
            Lib[BookNumber].Author = author;
            BookNumber++;
            }
        public void SearchByTitle(String title, Book[] Lib) {
            for (int i = 0; i < Lib.Length; i++) {
                if (Lib[i].Title.Equals(title))
                {
                    Console.WriteLine("Title:" + Lib[i].Title + " Author:" + Lib[i].Author);
                }
                else {
                    Console.WriteLine("Book not found.");
                }
            }
        }
        public void DeleteBook(int index, Book[] Lib) {
            if (index < 1000) {
                Lib[index].Title = null;
                Lib[index].Author = null;
            }
        }
    }
}
