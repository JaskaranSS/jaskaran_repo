﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Party_Manager
{
    enum Partytype { BirthdayParty =1,
                     AnniversaryParty,
                     RetirementParty }
    enum Venue { RadissonBlue=1 , HiltonHotel}
    enum Package { Silver = 1, Gold , Platinum }
    class Program
    {
        static void Main(string[] args)
        {
            PartyVenue p = new PartyVenue();
            p.PartyType();
            p.PartyPackage();
            p.DisplayDetails();
        }
    }
    public class Party {
        public string bookingType;
        public int noofPeople;
        public string partyVenue;
        public double decorationCharges;
        public double cateringCharges;
        public string package;
        public int ptypecost;
        public DateTime date;
        public double packageCharges;
        public double totalCharges;

        public virtual void PartyPackage() { }
        public virtual void DisplayDetails() { }
        public virtual void CalCharges(string ptype, int Noofpeople, string package) { }
    }

    public class PartyVenue : Party {
        public void PartyType()
        {   
            int n = 0;
            while (n == 0)
            {
                Console.WriteLine("Welcome to Successive Party Booking System");
                Console.WriteLine("\n" + "Choose type of party from below:");
                Console.WriteLine("1.BirthdayParty \n" +
                                  "2.AnniversaryParty \n" +
                                  "3.RetirementParty \n");
                try
                {
                    base.bookingType = Convert.ToString((Partytype)Convert.ToInt32(Console.ReadLine()));
                    n = 1;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Select a valid number!!");
                    n = 0;
                }
            }
            int x = 0;
            while (x == 0) 
            {
                try
                {
                    Console.WriteLine("Choose Venue:");
                    Console.Write("1.Radisson Blue\n" +
                                  "2.Hilton Hotel\n");
                    base.partyVenue = Convert.ToString((Venue)Convert.ToInt32(Console.ReadLine()));
                    x = 1;
                }
                catch (Exception e) {
                    Console.WriteLine("Choose a valid number!!");
                    x = 0;
                }
            }
                
            
            int z = 0;
            while (z == 0)
            {
                try
                {
                    Console.WriteLine("Enter date of event:");
                    base.date = Convert.ToDateTime(Console.ReadLine());
                    z = 1;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Please enter a valid date:");
                    z = 0;
                }
            }
        }
            
        
        public override void CalCharges(string ptype,int Noofpeople,string package ) {
            base.ptypecost = ptype.Equals("BirthdayParty") ? 50000 : ptype.Equals("AnniversaryParty") ? 70000 :
                             ptype.Equals("RetirementParty")? 100000: 0;

            base.decorationCharges = ptype.Equals("BirthdayParty") ? 10000 : ptype.Equals("AnniversaryParty") ? 20000 :
                                     ptype.Equals("RetirementParty") ? 30000 : 0;
            base.cateringCharges = ptype.Equals("BirthdayParty") ? (Noofpeople*10) : ptype.Equals("AnniversaryParty") ? (Noofpeople * 20) :
                                   ptype.Equals("RetirementParty") ? (Noofpeople * 30) : 0;
            base.packageCharges = package.Equals("Silver") ? 500 * Noofpeople : package.Equals("Gold") ? 1500 * Noofpeople:
                                  package.Equals("Platinum") ? 2500 * Noofpeople: 0;
            base.totalCharges = ptypecost + decorationCharges + cateringCharges + packageCharges;

        }
        public override void PartyPackage() {
            int nm = 0;
            while (nm == 0)
            {
                Console.WriteLine("Choose Party Package from below:");
                Console.Write("1.Silver(500 per person)\n" +
                              "2.Gold(1500 per person)\n" +
                              "3.Platinum(2500 per person)\n");
                try
                {
                    base.package = Convert.ToString((Package)Convert.ToInt32(Console.ReadLine()));
                    nm = 1;
                }
                catch (Exception e) {
                    Console.WriteLine("Enter a valid number!!");
                    nm = 0;
                }

                switch (package)
                {
                    case "Silver":
                        {
                            CalCharges(base.bookingType, base.noofPeople, base.package);
                        }
                        nm = 1;
                        break;
                    case "Gold":
                        {
                            CalCharges(base.bookingType, base.noofPeople, base.package);
                        }
                        nm = 1;
                        break;
                    case "Platinum":
                        {
                            CalCharges(base.bookingType, base.noofPeople, base.package);
                        }
                        nm = 1;
                        break;
                    default:
                        Console.WriteLine("Wrong input choose again!!");
                        break;
                }
                }
            int y = 0;
            while (y == 0)
            {
                try
                {
                    Console.WriteLine("Enter number of people:");
                    base.noofPeople = Convert.ToInt32(Console.ReadLine());
                    y = 1;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Enter a valid number!!");
                    y = 0;
                }
            }
        }
           
        public override void DisplayDetails()
        {
            Console.Write("BookingType :{0}\n"+
                           "Booking Venue :{1}\n" +
                           "Booking Date :{2}\n" +
                           "Package :{3}\n" +
                           "Catering Charges :{4}\n" +
                           "Decoration Charges :{5}\n" +
                           "Package Charges :{6}\n" +
                           "Number of People :{7}\n" +
                           "Total Charges :{8}",
                           base.bookingType, base.partyVenue, base.date , base.package, base.cateringCharges, base.decorationCharges, base.packageCharges,
                           base.noofPeople, base.totalCharges);
        }
    }
   }
