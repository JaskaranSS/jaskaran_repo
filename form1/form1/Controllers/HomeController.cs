﻿using form1.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace form1.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Index(string fname)
        {
            var model = new formModel();
            
            model.email = "abc@abc.com";
            model.lname = "Singh";
            model.gender = "male";
            model.mobile = "1234567890";
            model.higheredu = "Bachelors";
            model.password = "*******";
            string row = "<table>" +
                        "<tr><td>" + fname + "</td>" +
                        "<tr><td>" + model.lname + "</td>" +
                        "<tr><td>" + model.mobile + "</td>" +
                        "<tr><td>" + model.email + "</td>" +
                        "<tr><td>" + model.higheredu + "</td>" +
                        "<tr><td>" + model.gender + "</td>" +
                        "<tr><td>" + model.password + "</td></tr>" +
                            "</table>"; 
            return Content(row);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
