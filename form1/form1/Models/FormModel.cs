﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace form1.Models
{
    public class formModel
    {
        public string fname { get; set; }
        public string lname { get; set; }
        public string email { get; set; }
        public string higheredu { get; set; }
        public string address { get; set; }
        public string gender { get; set; }
        public string mobile { get; set; }
        public string password { get; set; }
    }
}
