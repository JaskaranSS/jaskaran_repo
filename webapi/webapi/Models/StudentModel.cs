﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webapi.Models
{
    public class StudentModel
    {
        public string name { get; set; }
        public int rollno { get; set; }

        public string status { get; set; }
    }
}
