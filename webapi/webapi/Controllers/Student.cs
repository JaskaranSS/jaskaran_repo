﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webapi.Models;

namespace webapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : Controller
    {
        
        List<StudentModel> students = new List<StudentModel>();
        public StudentController() {
            {
                students.Add(new StudentModel { name = "Jaskaran", rollno = 1, status = "Pass" });
                students.Add(new StudentModel { name = "Gaurav", rollno = 2, status = "Fail" });
            };
        }
        [HttpGet("{rollno:int}")]
        public Task<StudentModel> Get(int rollno) {
            var student = students.Find(s => s.rollno == rollno);
            return Task.FromResult(student);
        }
        [HttpPost]
        public Task<StudentModel> StudentPost(StudentModel stu)
        {
            return Task.FromResult(stu);
        }
    }
}
