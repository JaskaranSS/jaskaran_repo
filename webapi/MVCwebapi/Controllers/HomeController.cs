﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MVCwebapi.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using webapi.Models;

namespace MVCwebapi.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }
        public async Task<ActionResult> StudentResult(StudentViewModel stu)
        {
            string baseUrl= "https://localhost:44350/";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);
                var request = await client.GetAsync("api/Student/01");
                if (request.IsSuccessStatusCode)
                {
                    var result = request.Content.ReadAsStringAsync().Result;
                    stu = JsonConvert.DeserializeObject<StudentViewModel>(result);  
                }
                   
            }
            ViewBag.Rollno = stu.rollno;
            ViewBag.Name = stu.name;
            ViewBag.Status = stu.status;
            return View("Index", stu);
        }
        //public IActionResult Index()
        //{
        //    return View();
        //}

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
