﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DI.Resources
{
    public class Services:Itransient,Iscoped,Isingleton
    {
        private readonly Random _random = new Random();
        int number;
        public Services() {
            number = _random.Next();
        }
        public string GetNumber()
        {
            return number.ToString();
        }
    }
}
