﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DI.Models
{
    public class DImodelview
    {
        public string transient1 { get; set; }

        public string scoped1 { get; set; }

        public string singleton1 { get; set; }
        public string transient2 { get; set; }

        public string scoped2 { get; set; }

        public string singleton2 { get; set; }
    }
}
