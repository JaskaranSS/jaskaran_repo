﻿using DI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace DI.Controllers
{
    public class HomeController : Controller
    {
        private readonly Resources.Itransient _transient1;
        private readonly Resources.Iscoped _scoped1;
        private readonly Resources.Isingleton _singleton1;
        private readonly Resources.Itransient _transient2;
        private readonly Resources.Iscoped _scoped2;
        private readonly Resources.Isingleton _singleton2;
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger, Resources.Itransient transient1, Resources.Iscoped scoped1, Resources.Isingleton singleton1
                                                              , Resources.Itransient transient2, Resources.Iscoped scoped2, Resources.Isingleton singleton2)
        {
            _logger = logger;
            _transient1 = transient1;
            _scoped1 = scoped1;
            _singleton1 = singleton1;
            _transient2 = transient2;
            _scoped2 = scoped2;
            _singleton2 = singleton2;
        }

        public IActionResult Index(DImodelview di)
        {
            di.transient1 = _transient1.GetNumber();
            di.scoped1 = _scoped1.GetNumber();
            di.singleton1 = _singleton1.GetNumber();
            di.transient2 = _transient2.GetNumber();
            di.scoped2 = _scoped2.GetNumber();
            di.singleton2 = _singleton2.GetNumber();
            return View(di);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
