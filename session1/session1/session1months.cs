﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static session1.Session1colors;

namespace session1
{
    class session1months
    {
        static void Main(string[] args)
        {
            ArrayList arr = new ArrayList(){ "January", "February", "March", "April", "May", "June",
                                             "July" ,"August" ,"September" ,"October" ,"November" ,"December" };
            Console.WriteLine("Choose loop to find months starting with 'J':\n"
                +"1.For loop\n" +
                "2.For each loop\n" +
                "3.While loop\n" +
                "4.Session1Colors");
            int num = Convert.ToInt32(Console.ReadLine());
            switch (num) {
                case 1:
                    for (int i = 0; i < arr.Count; i++) {
                        String month = Convert.ToString(arr[i]);
                        char a = month[0];
                        if (a.Equals('J')) {
                            Console.WriteLine(month);
                        }
                    }
                    break;
                case 2:
                    foreach (var month in arr) {
                        String m = Convert.ToString(month);
                        char b = m[0];
                        if (b.Equals('J'))
                            {
                                Console.WriteLine(month);
                            }
                    }
                    break;
                case 3:
                    int j = 0;
                    while (j < arr.Count) {
                        String month = Convert.ToString(arr[j]);
                        char c = month[0];
                        if (c.Equals('J'))
                        {
                            Console.WriteLine(month);
                        }
                        j++;
                    }
                    break;
                case 4:
                    session1colors s1c = new session1colors();

                    break;
                    
            }
        }
    }
}
