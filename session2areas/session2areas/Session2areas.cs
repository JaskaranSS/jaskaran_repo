﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Csharp_training
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Choose the area you wish to find:\n" +
                               "1.Square\n" +
                               "2.Rectangle\n" +
                               "3.Circle\n" +
                               "4.Cuboid\n" +
                               "5.ReactangularPrism\n" +
                               "6.Sphere\n" +
                               "7.Cylinder\n" +
                               "8.Cone\n");
            int num = Convert.ToInt32(Console.ReadLine());
            switch (num) {
                case 1:
                    Console.WriteLine("Give side length");
                    double a = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine(areas.Squarearea(a));
                    break;
                case 2:
                    Console.WriteLine("Give length and breadth");
                    double b = Convert.ToDouble(Console.ReadLine());
                    double c = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine(areas.Reactanglearea(b ,c));
                    break;
                case 3:
                    Console.WriteLine("Give radius");
                    double d = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine(areas.Circlearea(d));
                    break;
                case 4:
                    Console.WriteLine("Give side length");
                    double e = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine(areas.Cuboidarea(e));
                    break;
                case 5:
                    Console.WriteLine("Give length, width and height");
                    double f = Convert.ToDouble(Console.ReadLine());
                    double g = Convert.ToDouble(Console.ReadLine());
                    double h = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine(areas.Cuboidarea(f, g ,h));
                    break;
                case 6:
                    Console.WriteLine("Give radius");
                    double i = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine(areas.Spherearea(i));
                    break;
                case 7:
                    Console.WriteLine("Give radius and height");
                    double j = Convert.ToDouble(Console.ReadLine());
                    double k = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine(areas.Cylinderarea(j, k));
                    break;
                case 8:
                    Console.WriteLine("Give radius and slant height");
                    double l = Convert.ToDouble(Console.ReadLine());
                    double m = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine(areas.Conearea(l, m));
                    break;
            }
        }
    }
    static class areas
    {
        static double pi = 3.14159265;
        public static double Squarearea(double side)
        {
            double area = side * side;
            return area;
        }
        public static double Reactanglearea(double a, double b)
        {
            double area = a * b;
            return area;
        }
        public static double Circlearea(double r)
        {
            double area = r * r * pi;
            return area;
        }
        public static double Cuboidarea(double side)
        {
            double area = side * side * 6;
            return area;
        }
        public static double Cuboidarea(double l, double w, double h)
        {
            double area = 2 * (w * l + l * h + h * w);
            return area;
        }
        public static double Spherearea(double r)
        {
            double area = 4 * pi * r * r;
            return area;
        }
        public static double Cylinderarea(double h, double r)
        {
            double area = 4 * pi * r * (r + h);
            return area;
        }
        public static double Conearea(double r, double slantheight)
        {
            double area = pi * r * (r + slantheight);
            return area;
        }
    }
}
