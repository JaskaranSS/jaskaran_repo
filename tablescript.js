// JavaScript source code
function FormClear() {
    console.log("formclear");
    document.getElementById("fname").value = "";
    document.getElementById("lname").value = "";
    document.getElementById("email").value = "";
    document.getElementById("mobile").value = "";
    document.getElementById("fname").focus();
    document.getElementsByName("gender").forEach(radio => {
        if (radio.checked) {
            radio.checked = false;
        }
    });
    document.getElementById("higheredu").value = "";
    document.getElementById("address").value = "";
    document.getElementById("password").value = "";
    document.getElementById("confirmpassword").value = "";
    }
var tbody = document.getElementById("infobody");
var Personarr = []
var rollno = 1;
function GetPerson() {
    var name = document.getElementById("fname").value;
    var email = document.getElementById("email").value;
    var mobile = document.getElementById("mobile").value;
    var gender;
    document.getElementsByName("gender").forEach(radio => {
        
        if (radio.checked) {
            gender = radio.value;
            console.log(gender);
        }
    });
    var higheredu = document.getElementById("higheredu").value;
    var address = document.getElementById("address").value;
    var password = document.getElementById("password").value;
    var PersonObj = {
        Rollno: rollno,
        Name: name,
        Email: email,
        Mobile: mobile,
        Gender: gender,
        Education: higheredu,
        Address: address,
        Password: password
    };
    Personarr.push(PersonObj);
    var row = document.createElement("tr");
    FormClear();
    Display(row, Personarr);
    rollno++;
}
function Display(row, obj) {
    rollnum = obj[obj.length -1].Rollno;
    Name = obj[obj.length -1].Name;
    Email = obj[obj.length -1].Email;
    Mobile = obj[obj.length -1].Mobile;
    Gender = obj[obj.length - 1].Gender;
    Education = obj[obj.length - 1].Education;
    Address = obj[obj.length -1].Address;
    Password = obj[obj.length -1].Password;
    row.innerHTML = `<td>${rollnum}</td>
            <td>${Name}</td>
            <td>${Email}</td>
            <td>${Mobile}</td>
            <td>${Gender}</td>
            <td>${Education}</td>
            <td>${Address}</td>
            <td>${Password}</td>
            <td><input type="button" id="del" class="button" value="Del" onclick="DeleteItem(this)">
            <input type="button" id="edit" class="button" value="Edit" onclick="UpdateItem(this)"></td>`;
    tbody.appendChild(row);
}
var register = document.getElementById("register");
register.addEventListener("click", function() {
    GetPerson();
});
function UpdateItem(Ebtn) {
    if (Ebtn.getAttribute("value") == "Update") {
        console.log("from update");
        console.log(Personarr[0].Rollno);
        var name = document.getElementById("fname").value;
        var email = document.getElementById("email").value;
        var mobile = document.getElementById("mobile").value;
        var gender;
        document.getElementsByName("gender").forEach(radio => {
            
            if (radio.checked) {
                gender = radio.value;
                console.log(gender);
            }
        });
        var higheredu = document.getElementById("higheredu").value;
        var address = document.getElementById("address").value;
        var password = document.getElementById("password").value;
        var cell = Ebtn.parentNode;
        var row = cell.parentNode;
        var cols = row.children;
        var rollno = cols[0].innerHTML;
        console.log(rollno);
        var obj = {
            Rollno: rollno,
            Name: name,
            Email: email,
            Mobile: mobile,
            Gender: gender,
            Education: higheredu,
            Address: address,
            Password: password
        };
        var activeobj;
        Personarr.forEach(function (item) {
            if (item.Rollno == obj.Rollno) {
                activeobj = item;
            }
        })
        console.log(activeobj)
        for (i = 0; i < obj.length; i++) {
            if (obj[i] == "" || obj[i] == undefined) {
                obj[i] = activeobj[i];
            }
        }
        console.log(obj)
        var newrow = document.createElement("tr");
        rollnum = obj.Rollno;
        Name = obj.Name;
        Email = obj.Email;
        Mobile = obj.Mobile;
        Gender = obj.Gender;
        Education = obj.Education;
        Address = obj.Address;
        Password = obj.Password;
        newrow.innerHTML = `<td>${rollnum}</td>
            <td>${Name}</td>
            <td>${Email}</td>
            <td>${Mobile}</td>
            <td>${Gender}</td>
            <td>${Education}</td>
            <td>${Address}</td>
            <td>${Password}</td>
            <td><input type="button" id="del" class="button" value="Del" onclick="DeleteItem(this)">
            <input type="button" id="edit" class="button" value="Edit" onclick="UpdateItem(this)"></td>`;
        tbody.insertBefore(newrow, row.nextSibling);
        row.remove();
        Ebtn.setAttribute("value", "Edit");
        FormClear();
    }
    if (Ebtn.getAttribute("value") == "Edit") {
        Ebtn.setAttribute("value", "Update");
        console.log("from edit");
    }
}
function DeleteItem(Dbtn) {
    var cell = Dbtn.parentNode;
    var row = cell.parentNode;
    row.remove();
}

    