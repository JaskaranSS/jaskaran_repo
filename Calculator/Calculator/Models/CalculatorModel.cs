﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Calculator.Models
{
    public class CalculatorModel
    {
        [Required]
        [RegularExpression(@"[0-9]*")]
        public double firstnumber { get; set; }
        [Required]
        [RegularExpression(@"[0-9]*")]
        public double secondnumber { get; set; }
        [Required]
        public string operation { get; set; }

        public double result { get; set; }
    }
}
