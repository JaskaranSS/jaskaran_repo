﻿using Calculator.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Calculator.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }
        
        
        public IActionResult Index()
        {
            var model = new CalculatorModel();
            return View(model);
        }
        public IActionResult Addition(CalculatorModel cm)
        {
            if (cm.operation.Equals("Subtraction"))
            {
                return RedirectToAction("Subtraction", cm);
            }
            else if (cm.operation.Equals("Division"))
            {
                return RedirectToAction("Division", cm);
            }
            else if (cm.operation.Equals("Multiplication"))
            {
                return RedirectToAction("Multiplication", cm);
            }
            if (ModelState.IsValid) {
                cm.result = cm.firstnumber + cm.secondnumber;
                return View("Index", cm);
            }

            return View("Index");
        }
        public IActionResult Subtraction(CalculatorModel cm)
        {
            if (ModelState.IsValid)
            {
                cm.result = cm.firstnumber - cm.secondnumber;
                return View("Index", cm);
            }
            return View("Index");
        }
        public IActionResult Division(CalculatorModel cm)
        {
            if (ModelState.IsValid)
            {
                if (cm.secondnumber != 0)
                {
                    cm.result = cm.firstnumber / cm.secondnumber;
                    return View("Index", cm);
                }
            }
            return View("Index");
        }
        public IActionResult Multiplication(CalculatorModel cm)
        {
            if (ModelState.IsValid)
            {
                cm.result = cm.firstnumber * cm.secondnumber;
                return View("Index", cm);
            }
            return View("Index");
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
