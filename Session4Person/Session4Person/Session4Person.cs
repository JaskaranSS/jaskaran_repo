﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Session4Person
{
    class session4person
    {
        static void Main(string[] args)
        {
            Person[] p1 = new Person[] {
                new Person{Name = "Anil", Age = 12 },
                new Person{Name = "Amit", Age = 120 },
                new Person{Name = "Anish", Age = 61 },
                new Person{Name = "Atul", Age = 55},
                new Person{Name = "Aniket", Age = 40 },
            };
            IEnumerable<Person> GetPersons(){
                return p1;
            }
            foreach (var person in GetPersons()) {
                if (person.Age > 60) {
                    Console.WriteLine("Name :{0}, Age :{1}", person.Name, person.Age);
                }
            }
        }
    }
    class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
    }
}
