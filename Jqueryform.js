// JavaScript source code
function FormClear() {
    console.log("formclear");
    $("#fname").val("");
    $("#lname").val("");
    $("#email").val("");
    $("#mobile").val("");
    $("#fname").focus();
    gender = $("input[name='gender']:checked").prop("checked", false);
    $("#higheredu").val("");
    $("#address").val("");
    $("#password").val("");
    $("#confirmpassword").val("");
}
var tbody = $("#infobody");
var Personarr = []
var rollno = 1;
function GetPerson() {
    var name = $("#fname").val();
    var email = $("#email").val();
    var mobile = $("#mobile").val();
    var gender;
    gender = $("input[name='gender']:checked").val();
    var higheredu = $("#higheredu").val();
    var address = $("#address").val();
    var password = $("#password").val();
    var PersonObj = {
        Rollno: rollno,
        Name: name,
        Email: email,
        Mobile: mobile,
        Gender: gender,
        Education: higheredu,
        Address: address,
        Password: password
    };
    Personarr.push(PersonObj);
    var $row = $("<tr></tr>");
    
    FormClear();
    Display($row, Personarr);
    rollno++;
}
function Display(row, obj) {
    rollnum = obj[obj.length - 1].Rollno;
    Name = obj[obj.length - 1].Name;
    Email = obj[obj.length - 1].Email;
    Mobile = obj[obj.length - 1].Mobile;
    Gender = obj[obj.length - 1].Gender;
    Education = obj[obj.length - 1].Education;
    Address = obj[obj.length - 1].Address;
    Password = obj[obj.length - 1].Password;
    row = $(`<td>${rollnum}</td>
            <td>${Name}</td>
            <td>${Email}</td>
            <td>${Mobile}</td>
            <td>${Gender}</td>
            <td>${Education}</td>
            <td>${Address}</td>
            <td>${Password}</td>
            <td><input type="button" id="del" class="button" value="Del" onclick="DeleteItem(this)">
            <input type="button" id="edit" class="button" value="Edit" onclick="UpdateItem(this)"></td>`);
    console.log(row);
    tbody.append(row);
}
$("#register").click(function () {
    GetPerson();
});
function UpdateItem(Ebtn) {
    if ($(Ebtn).attr("value") == "Update") {
        console.log("from update");
        console.log(Personarr[0].Rollno);
        var name = $("#fname").val();
        var email = $("#email").val();
        var mobile = $("#mobile").val();
        var gender;
        gender = $("input[name='gender']:checked").val();
        var higheredu = $("#higheredu").val();
        var address = $("#address").val();
        var password = $("#password").val();
        var rollno = $(Ebtn).parent("td").html;
        console.log(rollno);
        var obj = {
            Rollno: rollno,
            Name: name,
            Email: email,
            Mobile: mobile,
            Gender: gender,
            Education: higheredu,
            Address: address,
            Password: password
        };
        var activeobj;
        Personarr.forEach(function (item) {
            if (item.Rollno == obj.Rollno) {
                activeobj = item;
            }
        })
        console.log(activeobj)
        obj = function (obj) {
            Object.keys(obj).forEach(function (key) {
                if (obj[key] == "" || obj[key] == undefined) {
                    obj[key] = activeobj[key];
                }
            })
        }
        console.log(obj)
        var newrow = $("<tr></tr>");
        rollnum = obj.Rollno;
        Name = obj.Name;
        Email = obj.Email;
        Mobile = obj.Mobile;
        Gender = obj.Gender;
        Education = obj.Education;
        Address = obj.Address;
        Password = obj.Password;
        newrow = $(`<td>${rollnum}</td>
            <td>${Name}</td>
            <td>${Email}</td>
            <td>${Mobile}</td>
            <td>${Gender}</td>
            <td>${Education}</td>
            <td>${Address}</td>
            <td>${Password}</td>
            <td><input type="button" id="del" class="button" value="Del" onclick="DeleteItem(this)">
            <input type="button" id="edit" class="button" value="Edit" onclick="UpdateItem(this)"></td>`);
        var row = $(Ebtn).parent().closest("tr");
        tbody.before(newrow, row.nextSibling);
        row.remove();
        $(Ebtn).attr("value", "Edit");
        FormClear();
    }
    if ($(Ebtn).attr("value") == "Edit") {
        $(Ebtn).attr("value", "Update");
        console.log("from edit");
    }
}
function DeleteItem(Dbtn) {
    var row = $(Dbtn).parent().closest("tr");
    row.remove();
}
$("#demo").click(function () {
    $("#fname").val("jaskaran");
    $("#lname").val("singh");
    $("#email").val("abc@abc.com");
    $("#mobile").val("1234567890");
    gender = $("input[name='gender']:first").prop("checked", true);
    $("#higheredu option:eq(2)").prop("selected", true);
    $("#address").val("qwerty");
    $("#password").val("@a123456");
    $("#confirmpassword").val("@a123456");
})

