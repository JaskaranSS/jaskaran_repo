﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsharpTraining2
{
    public class Book 
    {
        public String book 
        {
            get;
            set;
        }
    }
    class Program
    {
        public delegate void alter(int a, Dictionary<int, Book> Book);
        void BookFinder2(int serialno, Dictionary<int, Book> Library)
        {
            foreach (KeyValuePair<int, Book> KP in Library)
            {
                if (KP.Key == serialno)
                {
                    Console.WriteLine("Found");
                }
                else Console.WriteLine("Not Found");
            }
        }
        static void Main(string[] args)
        {
            Book one = new Book();
            Book two = new Book();
            one.book = "Song of Ice and fire";
            two.book = "Lord of the Rings:Return of the king";
            Dictionary<int, Book> lib = new Dictionary<int, Book>();
            lib.Add(1, one);
            lib.Add(2, two);
            Program P = new Program();
            alter d1 = new alter(P.BookFinder2);
            d1(1, lib);
            2.BookFinder(lib);
            
        }
    }
    public static class Setup
    {
        public static void BookFinder(this int serialno, Dictionary<int, Book> Library)
        {
            foreach (KeyValuePair<int, Book> KP in Library) {
                if (KP.Key == serialno)
                {
                    Console.WriteLine("Found");
                }
                else Console.WriteLine("Not Found");
            }
        }
    }
}
