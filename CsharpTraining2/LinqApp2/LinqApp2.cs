﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqApp
{
    class Program
    {
        public class Employee
        {
            public int salary { get; set; }
            public string name { get; set; }
        }

        public class Department
        {
            public int DeptId { get; set; }
            public string name { get; set; }
        }

        static void Main(string[] args)
        {
            List<Employee> Emp = new List<Employee>() {
            new Employee(){name = "aaa" , salary = 2000},
            new Employee(){name = "aab" , salary = 3000},
            new Employee(){name = "aac" , salary = 4000},
            new Employee(){name = "aad" , salary = 5000},
            new Employee(){name = "aae" , salary = 6000},
            };
            var result = from e in Emp
                         where e.salary > 3000
                         select e.name;

            Employee emp = Emp.FirstOrDefault(e => e.salary > 2000);

            var numbers = Emp.Count(e => e.salary < 10000);

            var salaries = from e in Emp select e.salary;

            var orderby = from e in Emp orderby e.name select e;

            bool all = Emp.All(e => e.salary > 5000);

            var namelist = result.Aggregate((a, b) => a + " ," + b);

            int avgsal = (int)salaries.Average();

            List<Department> Dept = new List<Department>() {
            new Department(){name = "aaa" , DeptId = 1},
            new Department(){name = "aab" , DeptId = 2},
            new Department(){name = "aad" , DeptId = 4},
            new Department(){name = "aae" , DeptId = 5}
            };

            var EmployeeswithDept = Emp.Join(Dept,
                                    Employee => Employee.name,
                                    Department => Department.name,
                                    (Employee, Department) =>
                                    new { Id = Department.DeptId, Name = Employee.name }
                                    );
            Console.WriteLine(EmployeeswithDept);
            foreach (var Name in EmployeeswithDept)
            {
                Console.WriteLine(Name);
            }
            Console.WriteLine("\n\n");
            var EmpDept = (Emp.Select(s => s.name)).Intersect(Dept.Select(d => d.name));
            foreach (var Name in EmpDept)
            {

                Console.WriteLine(Name);
            }
            Console.ReadKey();
        }
    }
}