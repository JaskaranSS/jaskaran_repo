﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace session4months
{         
    enum months { Dummy, Jan, Feb, Mar, Apr, May, Jun,
                 Jul, Aug, Sep, Oct, Nov, Dec}

class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, int> keyValuePairs = new Dictionary<string, int>();
            for (int i = 0; i <= 12; i++) {
                if (i == 2)
                {
                    keyValuePairs.Add(Convert.ToString((months)i), 28);
                }
                else if (i % 2 == 0)
                {
                    keyValuePairs.Add(Convert.ToString((months)i), 30);
                }
                else {
                    keyValuePairs.Add(Convert.ToString((months)i), 31);
                }
            }
            Console.WriteLine("Enter the month:");
            string m = Console.ReadLine();
            foreach (KeyValuePair<string, int> month in keyValuePairs) {
                string n = month.Key;
                if (month.Key.Equals(m)) {
                    Console.WriteLine("Number of days:{0}", month.Value);
                }
            }
        }
    }
}
