﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using TaskApi.Models;
using TaskMvc.Models;

namespace TaskMvc.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> Index(IEnumerable<RecordViewModel> emps)
        {
            string baseurl = "https://localhost:44366/";

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseurl);
                var request = await client.GetAsync("api/1/TaskApi/GetAll");

                if (request.IsSuccessStatusCode)
                {
                    var result = request.Content.ReadAsStringAsync().Result;
                    emps = JsonConvert.DeserializeObject<IEnumerable<RecordViewModel>>(result);

                }
            }
                return View("Index", emps);
        }

        [HttpPost]
        public async Task<IActionResult> Search(string searchid)
        {
            string baseurl = "https://localhost:44366/";
            List<RecordViewModel> emp = new List<RecordViewModel>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseurl);
                var request = await client.GetAsync("api/1/TaskApi/"+searchid);

                if (request.IsSuccessStatusCode)
                {
                    var result = request.Content.ReadAsStringAsync().Result;
                    emp.Add(JsonConvert.DeserializeObject<RecordViewModel>(result));

                }
            }
            return View("Index", emp);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
