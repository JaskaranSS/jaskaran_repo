﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskApi.Models;
using TaskApi.Resources;

namespace TaskApi.Controllers
{
    [ApiController]
    [ApiVersion("2.0")]
    [Route("api/{v:apiVersion}/TaskApi")]
    public class TaskApi2Controller : Controller
    {
        private IResources _db; 
        public TaskApi2Controller(IResources db)
        {
            _db = db;
        }
        [HttpPost]
        [Route("Add")]
        public Task<List<RecordDbModel>> AddEmployee(RecordDbModel emp)
        {
            _db.AddRecord(emp);
            return Task.FromResult(_db.GetAll());
        }
        [HttpPost]
        [Route("Delete")]
        public Task<List<RecordDbModel>> DeleteEmployee(int id)
        {
            _db.DeleteRecord(id);
            return Task.FromResult(_db.GetAll());
        }

    }
}
