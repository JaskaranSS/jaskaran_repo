﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskApi.Models;
using TaskApi.Resources;

namespace TaskApi.Controllers
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/{v:apiVersion}/TaskApi")]
    public class TaskApiController : Controller
    {
        private IResources _db;
        public TaskApiController(IResources db) {
            _db = db;
        }
        [HttpGet("{id:int}")]
        public Task<RecordDbModel> GetEmployee(int id)
        {
            var record = _db.GetRecord(id);
            return Task.FromResult(record);
        }
        [HttpGet]
        [Route("GetAll")]
        public Task<List<RecordDbModel>> GetAllEmployees() {
            return Task.FromResult(_db.GetAll());
        }
    }
}
