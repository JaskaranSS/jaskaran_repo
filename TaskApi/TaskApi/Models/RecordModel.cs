﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskApi.Models
{
    public class RecordModel
    {
        public string name { get; set; }
        public int id { get; set; }
        public string department { get; set; }
    }
}
