﻿using EmpDb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskApi.Models;

namespace TaskApi.Resources
{
    public class Dbservice : IResources
    {
        private EmpContext _ec;
        public Dbservice(EmpContext ec) 
        {
            _ec = ec;
        }
        public void AddRecord(RecordDbModel record)
        {
            _ec.Employees.Add(record);
        }

        public void DeleteRecord(int id)
        {
            throw new NotImplementedException();
        }

        public List<RecordDbModel> GetAll()
        {
            return _ec.Employees.ToList();
        }

        public RecordDbModel GetRecord(int id)
        {
            throw new NotImplementedException();
        }
    }
}
