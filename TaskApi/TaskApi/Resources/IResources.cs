﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskApi.Models;

namespace TaskApi.Resources
{
    public interface IResources
    {
        public List<RecordDbModel> GetAll();
        public RecordDbModel GetRecord(int id);

        public void AddRecord(RecordDbModel record);
        public void DeleteRecord(int id);
    }
}
