﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskApi.Models;

namespace TaskApi.Resources
{
    public class ResourceService : IResources
    {
        List<RecordDbModel> Employees { get; set; }
        public ResourceService() {
            Employees = new List<RecordDbModel>();
            Employees.Add(new RecordDbModel { name = "Jaskaran", department = "Tech", id = 1 });
            Employees.Add(new RecordDbModel { name = "Karan", department = "HR", id = 2 });
            Employees.Add(new RecordDbModel { name = "Jack", department = "Sales", id = 3 });
        }
        public void AddRecord(RecordDbModel record)
        {
             Employees.Add(record);
        }

        public List<RecordDbModel> GetAll()
        {
            return Employees;
        }

        public RecordDbModel GetRecord(int id)
        {
            var record = Employees.Find(r => r.id == id);
            return record;
        }
        public void DeleteRecord(int id) 
        {
            var record = Employees.Find(r => r.id == id);
            Employees.Remove(record);
        }
    }
}
