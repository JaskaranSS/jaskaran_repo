﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TaskApi.Models
{
    public class RecordDbModel
    {
        [Required]
        public string name { get; set; }
        [Key]
        public int id { get; set; }
        public string department { get; set; }
    }
}
