﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Cshrp_training
{
    class Calc
    {
        static void Main(string[] args)
        {
            Calc cal = new Calc();
            int n = 0, caseno = 9;
            string m = "";
            while (n != 5)
            {
                m = cal.Options();
                try
                {
                    caseno = int.Parse(m);
                }
                catch (Exception e) {
                    Console.WriteLine("Enter a valid option number!!");
                    Console.WriteLine(""+ e.Message);
                    m = cal.Options();

                }
                
                string a = "", b = "";
                try
                {
                    a = Console.ReadLine();
                    b = Console.ReadLine();
                }
                catch (Exception e) {
                    Console.WriteLine("Enter valid numbers!!");
                    Console.WriteLine(""+ e.Message);
                }
                switch (caseno)
                {
                    case 1:
                        int sum = int.Parse(a) + int.Parse(b);
                        Console.WriteLine(sum);
                        break;
                    case 2:
                        int sub = int.Parse(a) - int.Parse(b);
                        Console.WriteLine(sub);
                        
                        break;
                    case 3:
                        int mul = int.Parse(a) * int.Parse(b);
                        Console.WriteLine(mul);
                        
                        break;
                    case 4:
                        if (a == "0" || b == "0")
                        {
                            Console.WriteLine("division not possible with 0!!");
                            break;
                        }
                        int div = int.Parse(a) / int.Parse(b);
                        Console.WriteLine(div);
                        
                        break;
                    case 5:
                        Console.WriteLine("Thank you\n" +
                                            "Calc will close in...");
                        
                        for (int i = 5; i > 0; i--) {
                            Console.WriteLine(i);
                            Thread.Sleep(1000);
                        }
                        System.Environment.Exit(0);
                        break;
                    default:
                        Console.WriteLine("Please enter valid numbers!!");
                        break;

                };
            };
        }
        public string Options() {
            int k = 0;
            string n = "9";
            while (k == 0)
            {
                Console.WriteLine("Please enter the corresponding number:\n" +
                                   "1.Add two numbers.\n" +
                                   "2.Subtract two numbers.\n" +
                                   "3.Multiply two numbers.\n" +
                                   "4.Divide two numbers\n" +
                                   "5.Exit");
                try
                {
                    n = Console.ReadLine();
                    k = 1;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Please enter a valid number");
                    Console.WriteLine("" + e.Message);
                }
            }
                return n;
            
        }
    }
}
