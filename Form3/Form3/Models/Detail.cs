﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace Form3.Models
{
    public class Detail
    {
        [Required]
        public string name { set; get; }
        [EmailAddress]
        public string email { set; get; }
        public string mobile { set; get; }
        public string address { set; get; }
        [Required]
        public string education { set; get; }
        [Required]
        public string gender { set; get; }
        [Required]
        [DataType(DataType.Password)]
        public string password { set; get; }
        [CompareAttribute("password")]
        public string confirmPassword { set; get; }

    }
}
