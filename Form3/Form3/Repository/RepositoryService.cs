﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Form3.Models;
namespace Form3.Repository
{
    public class RepositoryService : IRepository
    {
        public List<Detail> Person { get; set; }
        public RepositoryService(){
            var p1 = new Detail() {
                name = "Jaskaran",
                email = "abc@abc.com",
                mobile = "1234567890",
                address = "b11, port city",
                gender = "Male",
                education = "12th",
                password = "1234@"
            };
            var p2 = new Detail()
            {
                name = "Amit",
                email = "zxcabc@zxcabc.com",
                mobile = "1234567098",
                address = "c313, coastal city",
                gender = "Male",
                education = "Bachelors",
                password = "1234@1234"
            };
            var p3 = new Detail()
            {
                name = "Terry",
                email = "abcd@abcd.com",
                mobile = "4564567890",
                address = "d001, Sand city",
                gender = "Male",
                education = "Phd",
                password = "1234@@@@"
            };
            Person = new List<Detail>();
            Person.Add(p1);
            Person.Add(p2);
            Person.Add(p3);
        }

        public void AddPerson(Detail detailmodel)
        {
                Person.Add(detailmodel);
        }
    }
}
