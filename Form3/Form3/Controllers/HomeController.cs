﻿using Form3.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Form3.Repository;
namespace Form3.Controllers
{
    public class HomeController : Controller
    {
        private readonly IRepository repository;   
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger, IRepository repository)
        {
            _logger = logger;
            this.repository = repository;
        }
        
        
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Table(Detail info)
        {
            repository.AddPerson(info);
            var db = repository;
            return View("Table", db);
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
