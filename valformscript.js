// JavaScript source code
function ValForm() {
    var x = document.forms["taskform"]["fname"].value;
    if (x == "") {
        alert("First Name must be filled out");
        document.getElementById("errfname").innerHTML = "First Name must be filled out";
        return false;
    }
    if (x.length > 50) {
        alert("FirstName must be less than 50 characters long.");
        return false;
    }  
    var y = document.forms["taskform"]["lname"].value;
    if (y == "") {
        alert("Last Name must be filled out");
        return false;
    }
    if (x.length > 50) {
        alert("LastName must be less than 50 characters long.");
        return false;
    }  
    var z = document.forms["taskform"]["email"].value;
    if (z == "") {
        alert("E-mail must be filled out");
        return false;
    }
    var a = document.forms["taskform"]["mobile"].value;
    if (a == "") {
        alert("Mobile number must be filled out");
        return false;
    }
    var b = document.forms["taskform"]["address"].value;
    if (b == "") {
        alert("Address must be filled out");
        return false;
    }
}

function ValidateEmail() {
    var x = document.forms["taskform"]["email"].value;
    var atposition = x.indexOf("@");
    var dotposition = x.lastIndexOf(".");
    if (x == "") {
        document.getElementById("erremail").innerHTML="Email is required.";
    }
    if (atposition < 1 || dotposition < atposition + 2 || dotposition + 2 >= x.length) {
        alert("Please enter a valid e-mail address \n atpostion:" + atposition + "\n dotposition:" + dotposition);
        return false;
    }
}

function ValidateMobile() {
    
    var mobno = document.forms["taskform"]["mobile"].value;
    if (mobno.length > 10 || mobno.length < 10) {
        document.getElementById("errmobile").innerHTML = "Mobile number must be of 10 digits.";
        return false;
    }
    if (isNaN(mobno)) {
        document.getElementById("errmobile").innerHTML = "Mobile number can only contain digits.";
        return false;
    }
}

function ValidateConfirmPassword() {
    
    var password = document.forms["taskform"]["password"].value;
    var cpassword = document.forms["taskform"]["confirmpassword"].value;
    if (password != cpassword) {
        document.getElementById("errconfirmpassword").innerHTML = "Password doesn't match in confirm password.";
        return false;
    }
}

function ValidatePassword() {
    var specialformat = /[ `!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
    var alphaformat = /[a-zA-Z]/;
    var numformat = /[0-9]/;
    var password = document.forms["taskform"]["password"].value;
    if (password.match(specialformat) == null) {
        document.getElementById("specialchars").innerHTML = "Password must have special characters.";
    }
    
    if (password.match(alphaformat) == null) {
        document.getElementById("alphachars").innerHTML = "Password must have alphabets.";
    } 
    if (password.match(numformat) == null) {
        document.getElementById("numchars").innerHTML = "Password must have digits.";
    }
    if (password.length < 8) {
        alert("Password must be 8 character or more");
        return false;
    }
}
