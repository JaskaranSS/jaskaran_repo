﻿using FilterAndAreas.Student.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilterAndAreas.Areas.Student
{
    public class StudentController : Controller
    {
        [TypeFilter(typeof(CustomResultFilter))]
        public IActionResult Index()
        {
            var student = new StudentViewModel();
            return View(student);
        }

    }
}
