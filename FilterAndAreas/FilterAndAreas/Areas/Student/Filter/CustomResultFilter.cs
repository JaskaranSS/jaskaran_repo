﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilterAndAreas.Areas.Student
{
    public class CustomResultFilter:ResultFilterAttribute
    {
        private ILogger _logger;
        public CustomResultFilter(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger< CustomResultFilter>();
        }
        public override void OnResultExecuting(ResultExecutingContext context)
        {
            _logger.LogInformation("Adding mypageheader");
            //context.HttpContext.Request.Body.Write
            var headerName = "this is from filter";
            context.HttpContext.Response.Headers.Add(headerName, new string[] { "MyPageHeader" });

        }
        public override void OnResultExecuted(ResultExecutedContext context)
        {

            _logger.LogInformation("CustomResultFilter.OnResultExecuted");
        }
    }
}
