﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Session1colors
{
    class Program
    {
        static void Main(string[] args)
        {
			Console.WriteLine("Input the color:");
			String color = Console.ReadLine();
			Console.WriteLine("Choose search method:\n" +
								"1.Switch\n" +
								"2.If else\n" +
								"3.Ternary Operator");
			ArrayList arr = new ArrayList() { "red", "green", "blue", "RED", "GREEN", "BLUE", "Red", "Green", "Blue" };
			int num = Convert.ToInt32(Console.ReadLine());
			switch (num)
			{
				case 1:
					switch (color)
					{
						case "Red":
							Console.WriteLine("Colour found:", color);
							break;
						case "red":
							Console.WriteLine("Colour found:", color);
							break;
						case "RED":
							Console.WriteLine("Colour found:", color);
							break;
						case "Green":
							Console.WriteLine("Colour found:", color);
							break;
						case "GREEN":
							Console.WriteLine("Colour found:", color);
							break;
						case "green":
							Console.WriteLine("Colour found:", color);
							break;
						case "BLUE":
							Console.WriteLine("Colour found:", color);
							break;
						case "Blue":
							Console.WriteLine("Colour found:", color);
							break;
						case "blue":
							Console.WriteLine("Colour found:", color);
							break;
						default:
							Console.WriteLine("No matching colour found!!");
							break;
					}
					break;
				case 2:
					if (arr.Contains(color))
					{
						Console.WriteLine("matching colour found!!", color);
					}
					else Console.WriteLine("No matching colour found!!");
					break;
				case 3:
					String strg;
					strg = color.Equals("Red") ? "Found Red" : color.Equals("red") ? "Found red" : color.Equals("RED") ? "Found RED" :
						   color.Equals("Green") ? "Found Green" : color.Equals("green") ? "Found green" : color.Equals("GREEN") ? "Found GREEN" :
						   color.Equals("Blue") ? "Found Blue" : color.Equals("blue") ? "Found blue" : color.Equals("BLUE") ? "Found BLUE" : "Not found" ;
					Console.WriteLine(strg);
					break;
				default:
					Console.WriteLine("You didnot choose any given method :o");
					break;
			}
		}
    }
}
